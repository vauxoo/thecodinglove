# The Coding Love Crawler

Basically this is a crawler to download all links from thecodinglove.com and 
put the in a mongodb database in order to look for them locally and not remotelly.

The Idea is crawl all the page every few days in order to have it organized.

# How to install it.

```shell
$ git clone git@bitbucket.org:vauxoo/thecodinglove.git
$ cd thecodinglove
$ sudo pip install -r requirements.txt
$ ./run
```

# Roadmap:

1. Set a setup.py.
2. Ser a cronjob automatically from a config file when install.
3. Document the elements
