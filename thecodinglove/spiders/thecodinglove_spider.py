# -*- coding: UTF-8 -*-


import requests
from scrapy.spiders import SitemapSpider
from scrapy.selector import HtmlXPathSelector
from thecodinglove.items import ThecodingloveItem

class TheCodingLoveSpider(SitemapSpider):
    name = 'thecodinglove'
    sitemap_urls = ['http://thecodinglove.com/robots.txt']

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        item = ThecodingloveItem()
        item['_id'] = requests.utils.urlparse(response.url).path.split('/')[2]
        item['title'] = response.css('div#post1 .centre h3::text').extract()[0]
        item['gif'] = hxs.select("//div[@id='post1']//p/img/@src").extract()[0]
        item['link'] = response.url
        yield item
