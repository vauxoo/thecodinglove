from setuptools import setup, find_packages

setup(
    name='thecodinglove',
    version='1.5',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'scrapy': ['settings=thecodinglove.settings']
    },
    scripts=['run']
)
